function getHandScore(input: string):number{
    
    var result:number = 0
    var max:number = 0
    var checkSame:number = 0
    var checkNumSame:string = ''
    var heartsCount:number = 0
    var clubsCount:number = 0
    var diamondsCount:number = 0
    var spadesCount:number = 0
    const card:string[] = []
    const checkNum:string[] = []
    const checkMax:string[] = []
    var checkCard:string = ''
 
    const parts = input.split(" ");
    for (const part of parts) {
        card.push(part);
    }

    for(let i=0; i<card.length; i++){
        checkCard = card[i].charAt(0)
        checkNum.push(card[i].substring(1))
        if(checkCard == 'H'){
            if(card[i].substring(1) == 'A'){
                heartsCount += 11
            }else if(card[i].substring(1) == "K" || card[i].substring(1) == "Q" || card[i].substring(1) == "J"){
                heartsCount += 10
            }else{
                heartsCount += Number(card[i].substring(1))
            }
        }else if(checkCard == 'C'){
            if(card[i].substring(1) == 'A'){
                clubsCount += 11
            }else if(card[i].substring(1) == "K" || card[i].substring(1) == "Q" || card[i].substring(1) == "J"){
                clubsCount += 10
            }else{
                clubsCount += Number(card[i].substring(1))
            }
        }else if(checkCard == 'D'){
            if(card[i].substring(1) == 'A'){
                diamondsCount += 11
            }else if(card[i].substring(1) == "K" || card[i].substring(1) == "Q" || card[i].substring(1) == "J"){
                diamondsCount += 10
            }else{
                diamondsCount += Number(card[i].substring(1))
            }
        }else if(checkCard == 'S'){
            if(card[i].substring(1) == 'A'){
                spadesCount += 11
            }else if(card[i].substring(1) == "K" || card[i].substring(1) == "Q" || card[i].substring(1) == "J"){
                spadesCount += 10
            }else{
                spadesCount += Number(card[i].substring(1))
            }
        }
        checkMax.push(String(spadesCount), String(heartsCount), String(clubsCount), String(diamondsCount))
    }

    for(let i=0; i<checkNum.length; i++){
        // console.log(checkNum[i])
        if(i == 0){
            checkNumSame = checkNum[0]
            console.log(checkNum[i+1])
            if(checkNumSame == checkNum[i+1] && checkNum[i+1] != 'A'){
                checkSame++
            }else if(checkNumSame == checkNum[i+1] && checkNum[i+1] == 'A'){
                checkSame+=2
            }else if(checkNumSame != checkNum[i+1]){
                checkSame+=3
            }
        }else if(i < checkNum.length-1){
            if(checkNumSame == checkNum[checkNum.length-1] && checkNum[checkNum.length-1] != 'A'){
                checkSame++
            }else if(checkNumSame == checkNum[checkNum.length-1] && checkNum[checkNum.length-1] == 'A'){
                checkSame+=2
            }else if(checkNumSame != checkNum[checkNum.length-1]){
                checkSame+=3
            }
        }
    }

    switch(checkSame){
        case 2: 
            result = 32.5 
            break;
        case 4: 
            result = 35 
            break;
        default:
            for(let i=0; i<checkMax.length; i++){
                if(max < Number(checkMax[i])){
                    max = Number(checkMax[i])
                }
            }
            result = max
            break;
    }
    return result;
}

// const cards = "DQ SJ CJ"
// const cards = "DA SA CA"
const cards = "DA SA D10"
console.log(getHandScore(cards))