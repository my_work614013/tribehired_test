function getClockAngle(hh_mm:string):number{
    var hour:number = Number(hh_mm.substring(0, 2))
    var minute:number = Number(hh_mm.substring(3, 5))
    var hourDegrees:number = (hour * 30) + (minute * 30.0 / 60)
    var minuteDegrees:number = minute * 6
    var diffDegrees:number = Math.abs(hourDegrees - minuteDegrees)

    if(diffDegrees > 180){
        diffDegrees = 360 - diffDegrees
    }
    return diffDegrees
}
// const time = "9.00"
const time = "17.30"
console.log(getClockAngle(time))