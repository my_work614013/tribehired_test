"use strict";
function getQuestionPart(phrases) {
    var result = [];
    const word = [];
    var check = '';
    const removeSpace = phrases.map(phrase => phrase.replace(/\s+/g, ''));
    for (let i = 0; i < removeSpace.length; i++) {
        for (let j = 1; j <= removeSpace[i].length; j++) {
            const subString = removeSpace[i].substring(0, j);
            const checkSubString = removeSpace.filter(word => word.includes(subString)).length;
            if (checkSubString == removeSpace.length) {
                word.push(subString);
            }
        }
    }
    if (word.length % 2 == 0) {
        check = word[word.length - 1];
    }
    else {
        check = word[word.length - 2];
    }
    for (let i = 0; i < removeSpace.length; i++) {
        result.push(removeSpace[i].replace(check, ""));
    }
    return result;
}
// const phrases = ["BEFRIEND", "GIRLFRIEND", "FRIENDSHIP"];
// const phrases = ["PLAYFUL", "PLAYER", "PLAYPROUND"];
const phrases = ["BATHROOM", "BATH SALT", "BLOODBATH"];
console.log(getQuestionPart(phrases));
