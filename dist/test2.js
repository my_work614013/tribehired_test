"use strict";
function getClockAngle(hh_mm) {
    var hour = Number(hh_mm.substring(0, 2));
    var minute = Number(hh_mm.substring(3, 5));
    var hourDegrees = (hour * 30) + (minute * 30.0 / 60);
    var minuteDegrees = minute * 6;
    var diffDegrees = Math.abs(hourDegrees - minuteDegrees);
    if (diffDegrees > 180) {
        diffDegrees = 360 - diffDegrees;
    }
    return diffDegrees;
}
// const time = "9.00"
const time = "17.30";
console.log(getClockAngle(time));
